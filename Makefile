
# exécuter les commandes principales
all: xml dtd xsd web xq

# vérifier que le fichier xml est bien formé
xml: masters-info.xml
	xmllint --noout masters-info.xml

# vérifier que le fichier xml est valide grâce à la DTD
dtd: masters-info.xml
	xmllint --valid --noout masters-info.xml

# vérifier que le fichier xml est valide grâce au schema XSD
xsd:
	xmllint --noent --noout --schema xsd/masters-info.xsd masters-info.xml

# générer l'ensemble du site web
web:
	mkdir -p www www/enseignants www/parcours www/specialites www/UEs
	java -cp saxon9he.jar net.sf.saxon.Transform -a -t -s:masters-info.xml
	#xsltproc xsl/masters-info.xsl masters-info.xml

# création d'une page enseignants grâce à une requête XQuery
xq:
	java -cp saxon9he.jar net.sf.saxon.Query -s:"masters-info.xml" -q:"xq.txt" -o:"www/xq.html"

# vérification de conformité aux standards avec tidy
tidy:
	tidy -utf8 www/*.html

# netoyage du repertoire
clean:
	find www/ -name \*.html -type f -exec rm -f {} +
