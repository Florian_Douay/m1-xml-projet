<?xml version="1.0" encoding="UTF-8" ?>
<xsl:stylesheet version="2.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:fd="florian.douay@univ-amu.fr" exclude-result-prefixes="xs fd">

    <xsl:output method="html" encoding="UTF-8" indent="yes"/>


    <xsl:template match="/">
        <xsl:call-template name="generate-index"/>
        <xsl:call-template name="generate-specialites"/>
        <xsl:call-template name="generate-enseignants"/>
        <xsl:call-template name="generate-UEs"/>
        <xsl:call-template name="generate-parcours"/>
        <xsl:call-template name="generate-page-all-enseignants"/>
        <xsl:call-template name="generate-page-all-UEs"/>
    </xsl:template>


    <xsl:template name="generate-index">
        <xsl:result-document href="www/index.html" method="html" omit-xml-declaration = "yes">
        <xsl:text disable-output-escaping='yes'>&lt;!DOCTYPE html&gt;</xsl:text>
        <html>
            <head>
                <meta charset="utf-8"/>
                <title>
                <xsl:text>Présentation du Master Informatique</xsl:text>
                </title>
                <link rel="stylesheet" type="text/css" href="css/masters.css" />
                <link rel="stylesheet" type="text/css" href="css/menu.css" />
                <script src="script/jquery-was-latest.min.js" type="text/javascript"></script>
                <script src="script/menu.js" type="text/javascript"></script>
            </head>
            <body>
                <div class="main-container">
                <header>
                    <nav class="nav-images">
                        <a href="http://sciences.univ-amu.fr/" class="header-univmed" />
                        <a href="index.html" class="header-masterinfo" />
                        <a href="http://www.univ-amu.fr/" class="header-univprov" />
                    </nav>
                    <nav class="nav-menu">
                        <div id='cssmenu' class="align-center">
                        <ul>
                           <li class="active"><a href='index.html'><xsl:text>Le Master</xsl:text></a></li>
                           <li class='has-sub middle-li'><a href='#'><xsl:text>Les formations</xsl:text></a>
                              <ul>
                                  <xsl:call-template name="menu-formation">
                                      <xsl:with-param name="path-parcours">parcours/</xsl:with-param>
                                  </xsl:call-template>
                              </ul>
                           </li>
                           <li class="middle-li"><a href='unités.html'><xsl:text>Les unités</xsl:text></a></li>
                           <li><a href='intervenants.html'><xsl:text>Les intervenants</xsl:text></a></li>
                        </ul>
                        </div>
                    </nav>
                </header>
                <main>
                <section id="header-pres-masters">
                    <h2 class="align-center"><xsl:text>Master Informatique de Marseille</xsl:text></h2>
                    <p class="align-center bold"><xsl:text>habilitation 2012 / 2018</xsl:text></p>
                    <p class="align-center">
                        <a href="http://dii.univ-mrs.fr/" class="underline bold"><xsl:text>Département Informatique et Interactions </xsl:text></a>
                        <br/>
                        <a href="http://sciences.univ-amu.fr/" class="underline bold"><xsl:text>Faculté des Sciences</xsl:text></a>
                    </p>
                    <p class="align-center">
                        <a href="http://www.univ-amu.fr/" class="underline bold"><xsl:text>Aix-Marseille Université</xsl:text></a>
                    </p>
                </section>
                <section>
                    <h3><xsl:text>Présentation du Master Informatique</xsl:text></h3>
                    <xsl:for-each select="/masters/présentation">
                        <xsl:apply-templates />
                    </xsl:for-each>
                </section>
                <section>
                    <h3><xsl:text>Spécialités et options</xsl:text></h3>
                        <xsl:call-template name="display-formation-tab" />
                </section>
                <section>
                    <h3><xsl:text>Débouchés et effectifs</xsl:text></h3>
                    <xsl:for-each select="/masters/débouchées">
                        <xsl:apply-templates />
                    </xsl:for-each>
                </section>
                </main>
                </div>
            </body>
        </html>
        </xsl:result-document>
    </xsl:template>


    <xsl:template name="generate-specialites">
        <xsl:for-each select="/masters/années/année/spécialité">
        <xsl:result-document href="www/specialites/{fd:formatFileName(intitulé)}.html" omit-xml-declaration = "yes">
        <xsl:text disable-output-escaping='yes'>&lt;!DOCTYPE html&gt;</xsl:text>
        <html>
            <head>
                <meta charset="utf-8"/>
                <title>
                    <xsl:value-of select="intitulé"/>
                </title>
                <link rel="stylesheet" type="text/css" href="../css/masters.css" />
                <link rel="stylesheet" type="text/css" href="../css/menu.css" />
                <script src="../script/jquery-was-latest.min.js" type="text/javascript"></script>
                <script src="../script/menu.js" type="text/javascript"></script>
            </head>
            <body>
                <div class="main-container">
                <header>
                    <nav class="nav-images">
                        <a href="http://sciences.univ-amu.fr/" class="header-univmed" />
                        <a href="../index.html" class="header-masterinfo" />
                        <a href="http://www.univ-amu.fr/" class="header-univprov" />
                    </nav>
                    <nav class="nav-menu">
                        <div id='cssmenu' class="align-center">
                        <ul>
                           <li><a href='../index.html'><xsl:text>Le Master</xsl:text></a></li>
                           <li class='active has-sub middle-li'><a href='#'><xsl:text>Les formations</xsl:text></a>
                              <ul>
                                  <xsl:call-template name="menu-formation">
                                      <xsl:with-param name="path-parcours"></xsl:with-param>
                                  </xsl:call-template>
                              </ul>
                           </li>
                           <li class="middle-li"><a href='../unités.html'><xsl:text>Les unités</xsl:text></a></li>
                           <li><a href='../intervenants.html'><xsl:text>Les intervenants</xsl:text></a></li>
                        </ul>
                        </div>
                    </nav>
                </header>
                <main>
                <section>
                    <h2 class="align-center"><xsl:value-of select="intitulé"/></h2>
                </section>

                <section>
                    <h3><xsl:text>Présentation et objectifs</xsl:text></h3>

                    <xsl:call-template name="display-resp-lieux" />

                    <xsl:if test="prés-objectifs">
                        <xsl:apply-templates select="prés-objectifs" />
                    </xsl:if>
                </section>

                <xsl:if test="compétences">
                    <section>
                        <h3><xsl:text>Compétences à acquérir</xsl:text></h3>
                        <xsl:apply-templates select="compétences" />
                    </section>
                </xsl:if>

                <xsl:if test="connaissances">
                    <section>
                        <h3><xsl:text>Connaissances à acquérir</xsl:text></h3>
                        <xsl:apply-templates select="connaissances" />
                    </section>
                </xsl:if>

                <xsl:if test="count(parcours) = 1">
                    <xsl:call-template name="programme">
                        <xsl:with-param name="path-parcours" select="parcours"/>
                    </xsl:call-template>
                </xsl:if>

                <xsl:if test="politique-stages">
                    <section>
                        <h3><xsl:text>Politique des stages</xsl:text></h3>
                        <xsl:apply-templates select="politique-stages" />
                    </section>
                </xsl:if>

                <xsl:if test="formation-recherche">
                    <section>
                        <h3><xsl:text>Aspects formation et recherche</xsl:text></h3>
                        <xsl:apply-templates select="formation-recherche" />
                    </section>
                </xsl:if>

                <xsl:if test="ens-délocalisé">
                    <section>
                        <h3><xsl:text>Enseignements délocalisés</xsl:text></h3>
                        <xsl:apply-templates select="ens-délocalisé" />
                    </section>
                </xsl:if>
                <xsl:if test="modalités-pédago">
                    <section>
                        <h3><xsl:text>Modalités pédagogiques</xsl:text></h3>
                        <xsl:apply-templates select="modalités-pédago" />
                    </section>
                </xsl:if>

                <xsl:if test="cond-prérequis">
                    <section>
                        <h3><xsl:text>Conditions d'admission et pré-requis</xsl:text></h3>
                        <xsl:apply-templates select="cond-prérequis" />
                    </section>
                </xsl:if>

                <xsl:if test="débouchées">
                    <section>
                        <h3><xsl:text>Débouchés</xsl:text></h3>
                        <xsl:apply-templates select="débouchées" />
                    </section>
                </xsl:if>

                <xsl:if test="poursuite-études">
                    <section>
                        <h3><xsl:text>Poursuite d'études</xsl:text></h3>
                        <xsl:apply-templates select="poursuite-études" />
                    </section>
                </xsl:if>
                </main>
                </div>
            </body>
        </html>
        </xsl:result-document>
        </xsl:for-each>
    </xsl:template>


    <xsl:template name="generate-parcours">
        <xsl:for-each select="/masters/années/année/spécialité/parcours">
        <xsl:result-document href="www/parcours/{fd:formatFileName(intitulé)}.html" omit-xml-declaration = "yes">
        <xsl:text disable-output-escaping='yes'>&lt;!DOCTYPE html&gt;</xsl:text>
        <html>
            <head>
                <meta charset="utf-8"/>
                <title>
                <xsl:value-of select="intitulé"/>
                </title>
                <link rel="stylesheet" type="text/css" href="../css/masters.css" />
                <link rel="stylesheet" type="text/css" href="../css/menu.css" />
                <script src="../script/jquery-was-latest.min.js" type="text/javascript"></script>
                <script src="../script/menu.js" type="text/javascript"></script>
            </head>
            <body>
                <div class="main-container">
                <header>
                    <nav class="nav-images">
                        <a href="http://sciences.univ-amu.fr/" class="header-univmed" />
                        <a href="../index.html" class="header-masterinfo" />
                        <a href="http://www.univ-amu.fr/" class="header-univprov" />
                    </nav>
                    <nav class="nav-menu">
                        <div id='cssmenu' class="align-center">
                        <ul>
                           <li><a href='../index.html'><xsl:text>Le Master</xsl:text></a></li>
                           <li class='active has-sub middle-li'><a href='#'><xsl:text>Les formations</xsl:text></a>
                              <ul>
                                  <xsl:call-template name="menu-formation">
                                      <xsl:with-param name="path-parcours"></xsl:with-param>
                                  </xsl:call-template>
                              </ul>
                           </li>
                           <li class="middle-li"><a href='../unités.html'><xsl:text>Les unités</xsl:text></a></li>
                           <li><a href='../intervenants.html'><xsl:text>Les intervenants</xsl:text></a></li>
                        </ul>
                        </div>
                    </nav>
                </header>
                <main>
                <section>
                    <h2 class="align-center"><xsl:value-of select="intitulé"/></h2>
                </section>

                <section>
                    <h3><xsl:text>Présentation et objectifs</xsl:text></h3>

                    <xsl:call-template name="display-resp-lieux" />

                    <xsl:if test="prés-objectifs">
                        <xsl:apply-templates select="prés-objectifs" />
                    </xsl:if>
                </section>

                <xsl:if test="compétences">
                    <section>
                        <h3><xsl:text>Compétences à acquérir</xsl:text></h3>
                        <xsl:apply-templates select="compétences" />
                    </section>
                </xsl:if>

                <xsl:if test="connaissances">
                    <section>
                        <h3><xsl:text>Connaissances à acquérir</xsl:text></h3>
                        <xsl:apply-templates select="connaissances" />
                    </section>
                </xsl:if>

                <xsl:call-template name="programme">
                    <xsl:with-param name="path-parcours" select="self::node()"/>
                </xsl:call-template>

                <xsl:if test="politique-stages">
                    <section>
                        <h3><xsl:text>Politique des stages</xsl:text></h3>
                        <xsl:apply-templates select="politique-stages" />
                    </section>
                </xsl:if>

                <xsl:if test="formation-recherche">
                    <section>
                        <h3><xsl:text>Aspects formation et recherche</xsl:text></h3>
                        <xsl:apply-templates select="formation-recherche" />
                    </section>
                </xsl:if>

                <xsl:if test="ens-délocalisé">
                    <section>
                        <h3><xsl:text>Enseignements délocalisés</xsl:text></h3>
                        <xsl:apply-templates select="ens-délocalisé" />
                    </section>
                </xsl:if>
                <xsl:if test="modalités-pédago">
                    <section>
                        <h3><xsl:text>Modalités pédagogiques</xsl:text></h3>
                        <xsl:apply-templates select="modalités-pédago" />
                    </section>
                </xsl:if>

                <xsl:if test="cond-prérequis">
                    <section>
                        <h3><xsl:text>Conditions d'admission et pré-requis</xsl:text></h3>
                        <xsl:apply-templates select="cond-prérequis" />
                    </section>
                </xsl:if>

                <xsl:if test="débouchées">
                    <section>
                        <h3><xsl:text>Débouchés</xsl:text></h3>
                        <xsl:apply-templates select="débouchées" />
                    </section>
                </xsl:if>

                <xsl:if test="poursuite-études">
                    <section>
                        <h3><xsl:text>Poursuite d'études</xsl:text></h3>
                        <xsl:apply-templates select="poursuite-études" />
                    </section>
                </xsl:if>
                </main>
                </div>
            </body>
        </html>
        </xsl:result-document>
        </xsl:for-each>
    </xsl:template>


    <xsl:template name="generate-UEs">
        <xsl:for-each select="/masters/UEs/UE">
        <xsl:result-document href="www/UEs/{fd:formatFileName(@id)}.html" omit-xml-declaration = "yes">
        <xsl:text disable-output-escaping='yes'>&lt;!DOCTYPE html&gt;</xsl:text>
        <html>
            <head>
                <meta charset="utf-8"/>
                <title>
                <xsl:value-of select="intitulé"/>
                </title>
                <link rel="stylesheet" type="text/css" href="../css/masters.css" />
                <link rel="stylesheet" type="text/css" href="../css/menu.css" />
                <script src="../script/jquery-was-latest.min.js" type="text/javascript"></script>
                <script src="../script/menu.js" type="text/javascript"></script>
            </head>
            <body>
                <div class="main-container">
                <header>
                    <nav class="nav-images">
                        <a href="http://sciences.univ-amu.fr/" class="header-univmed" />
                        <a href="../index.html" class="header-masterinfo" />
                        <a href="http://www.univ-amu.fr/" class="header-univprov" />
                    </nav>
                    <nav class="nav-menu">
                        <div id='cssmenu' class="align-center">
                        <ul>
                           <li><a href='../index.html'><xsl:text>Le Master</xsl:text></a></li>
                           <li class='has-sub middle-li'><a href='#'><xsl:text>Les formations</xsl:text></a>
                              <ul>
                                  <xsl:call-template name="menu-formation">
                                      <xsl:with-param name="path-parcours">../parcours/</xsl:with-param>
                                  </xsl:call-template>
                              </ul>
                           </li>
                           <li class="active middle-li"><a href='../unités.html'><xsl:text>Les unités</xsl:text></a></li>
                           <li><a href='../intervenants.html'><xsl:text>Les intervenants</xsl:text></a></li>
                        </ul>
                        </div>
                    </nav>
                </header>
                <main>
                <section>
                    <h2 class="align-center">
                        <xsl:value-of select="intitulé"/>
                        <xsl:text> (</xsl:text>
                        <xsl:value-of select="crédits" />
                        <xsl:text> crédits)</xsl:text>
                    </h2>
                </section>
                <section>
                    <h3><xsl:text>Description</xsl:text></h3>
                    <xsl:choose>
                        <xsl:when test="présentation">
                            <xsl:apply-templates select="présentation" />
                        </xsl:when>
                        <xsl:otherwise>
                            <p>
                                <xsl:text>Nous sommes désolé, mais nous n'avons pas encore de résumé.</xsl:text>
                            </p>
                        </xsl:otherwise>
                    </xsl:choose>
                </section>
                <xsl:if test="prérequis">
                    <section>
                        <h3><xsl:text>Prérequis</xsl:text></h3>
                        <xsl:apply-templates select="prérequis" />
                    </section>
                </xsl:if>
                <section>
                    <h3><xsl:text>Apparaît dans les parcours</xsl:text></h3>
                    <xsl:call-template name="extractParcoursAvecUE">
                        <xsl:with-param name="ue-id" select="@id"/>
                    </xsl:call-template>
                </section>
                </main>
                </div>
            </body>
        </html>
        </xsl:result-document>
        </xsl:for-each>
    </xsl:template>


    <xsl:template name="generate-enseignants">
        <xsl:for-each select="/masters/enseignants/enseignant">
        <xsl:result-document href="www/enseignants/{fd:formatFileName(concat(nom,'_',prénom))}.html" omit-xml-declaration = "yes">
        <xsl:text disable-output-escaping='yes'>&lt;!DOCTYPE html&gt;</xsl:text>
        <html>
            <head>
                <meta charset="utf-8"/>
                <title>
                    <xsl:value-of select="prénom"/>
                    <xsl:text> </xsl:text>
                    <xsl:value-of select="nom"/>
                </title>
                <link rel="stylesheet" type="text/css" href="../css/masters.css" />
                <link rel="stylesheet" type="text/css" href="../css/menu.css" />
                <script src="../script/jquery-was-latest.min.js" type="text/javascript"></script>
                <script src="../script/menu.js" type="text/javascript"></script>
            </head>
            <body>
                <div class="main-container">
                <header>
                    <nav class="nav-images">
                        <a href="http://sciences.univ-amu.fr/" class="header-univmed" />
                        <a href="../index.html" class="header-masterinfo" />
                        <a href="http://www.univ-amu.fr/" class="header-univprov" />
                    </nav>
                    <nav class="nav-menu">
                        <div id='cssmenu' class="align-center">
                        <ul>
                           <li><a href='../index.html'><xsl:text>Le Master</xsl:text></a></li>
                           <li class='has-sub middle-li'><a href='#'><xsl:text>Les formations</xsl:text></a>
                              <ul>
                                  <xsl:call-template name="menu-formation">
                                      <xsl:with-param name="path-parcours">../parcours/</xsl:with-param>
                                  </xsl:call-template>
                              </ul>
                           </li>
                           <li class="middle-li"><a href='../unités.html'><xsl:text>Les unités</xsl:text></a></li>
                           <li class="active"><a href='../intervenants.html'><xsl:text>Les intervenants</xsl:text></a></li>
                        </ul>
                        </div>
                    </nav>
                </header>
                <main>
                <section>
                    <h2 class="align-center">
                        <xsl:value-of select="prénom"/>
                        <xsl:text> </xsl:text>
                        <xsl:value-of select="nom"/>
                    </h2>
                </section>
                <section>
                    <h3><xsl:text>Informations</xsl:text></h3>
                    <table>
                        <tbody>
                            <tr>
                                <th><xsl:text>Nom</xsl:text></th>
                                <td><xsl:value-of select="nom" /></td>
                            </tr>
                            <tr>
                                <th><xsl:text>Prénom</xsl:text></th>
                                <td><xsl:value-of select="prénom" /></td>
                            </tr>
                            <tr>
                                <th><xsl:text>Adresse e-mail</xsl:text></th>
                                <td><xsl:value-of select="email" /></td>
                            </tr>
                        </tbody>
                    </table>
                </section>
                <section>
                    <h3><xsl:text>Responsabilités</xsl:text></h3>

                    <xsl:variable name="var-liste-responsabilités">
                        <xsl:call-template name="extractResponsableParcours">
                            <xsl:with-param name="enseignant-id" select="@id"/>
                        </xsl:call-template>
                    </xsl:variable>

                    <xsl:choose>
                        <xsl:when test="$var-liste-responsabilités/node()">
                            <ul>
                                <xsl:for-each select="$var-liste-responsabilités/node()">
                                    <li><xsl:apply-templates select="." /></li>
                                </xsl:for-each>
                            </ul>
                        </xsl:when>
                        <xsl:otherwise>
                            <p>
                                <xsl:value-of select="prénom" />
                                <xsl:text> </xsl:text>
                                <xsl:value-of select="nom" />
                                <xsl:text> n'est actuellement responsable d'aucun parcours et d'aucune spécialité.</xsl:text>
                            </p>
                        </xsl:otherwise>
                    </xsl:choose>
                </section>
                </main>
                </div>
            </body>
        </html>
        </xsl:result-document>
        </xsl:for-each>
    </xsl:template>


    <xsl:template name="generate-page-all-UEs">
        <xsl:result-document href="www/unités.html" omit-xml-declaration = "yes">
        <xsl:text disable-output-escaping='yes'>&lt;!DOCTYPE html&gt;</xsl:text>
        <html>
            <head>
                <meta charset="utf-8"/>
                <title>
                    <xsl:text>Les unités</xsl:text>
                </title>
                <link rel="stylesheet" type="text/css" href="css/masters.css" />
                <link rel="stylesheet" type="text/css" href="css/menu.css" />
                <script src="script/jquery-was-latest.min.js" type="text/javascript"></script>
                <script src="script/menu.js" type="text/javascript"></script>
            </head>
            <body>
                <div class="main-container">
                <header>
                    <nav class="nav-images">
                        <a href="http://sciences.univ-amu.fr/" class="header-univmed" />
                        <a href="index.html" class="header-masterinfo" />
                        <a href="http://www.univ-amu.fr/" class="header-univprov" />
                    </nav>
                    <nav class="nav-menu">
                        <div id='cssmenu' class="align-center">
                        <ul>
                           <li><a href='index.html'><xsl:text>Le Master</xsl:text></a></li>
                           <li class='has-sub middle-li'><a href='#'><xsl:text>Les formations</xsl:text></a>
                              <ul>
                                  <xsl:call-template name="menu-formation">
                                      <xsl:with-param name="path-parcours">parcours/</xsl:with-param>
                                  </xsl:call-template>
                              </ul>
                           </li>
                           <li class="active middle-li"><a href='unités.html'><xsl:text>Les unités</xsl:text></a></li>
                           <li><a href='intervenants.html'><xsl:text>Les intervenants</xsl:text></a></li>
                        </ul>
                        </div>
                    </nav>
                </header>
                <main>
                <section>
                    <h2 class="align-center">
                        <xsl:text>Les unités</xsl:text>
                    </h2>
                </section>
                <section>
                    <h3><xsl:text>Liste des unités</xsl:text></h3>
                    <table>
                        <thead>
                            <tr>
                                <th><xsl:text>Intitulé</xsl:text></th>
                                <th><xsl:text>Crédits</xsl:text></th>
                                <th><xsl:text>Page</xsl:text></th>
                                <th><xsl:text>Code</xsl:text></th>
                            </tr>
                        </thead>
                        <tbody>
                            <xsl:for-each select="/masters/UEs/UE">
                                <tr>
                                    <td>
                                        <xsl:value-of select="intitulé" />
                                    </td>
                                   <td>
                                        <xsl:value-of select="crédits" />
                                    </td>
                                    <td>
                                        <a href="UEs/{fd:formatFileName(@id)}.html">
                                            <img src="css/img/logo-page.png" alt="fiche_UE" />
                                        </a>
                                    </td>
                                    <td>
                                        <span class="italic"><xsl:value-of select="@id" /></span>
                                    </td>
                                </tr>
                            </xsl:for-each>
                        </tbody>
                    </table>
                </section>
                </main>
                </div>
            </body>
        </html>
        </xsl:result-document>
    </xsl:template>


    <xsl:template name="generate-page-all-enseignants">
        <xsl:result-document href="www/intervenants.html" omit-xml-declaration = "yes">
        <xsl:text disable-output-escaping='yes'>&lt;!DOCTYPE html&gt;</xsl:text>
        <html>
            <head>
                <meta charset="utf-8"/>
                <title>
                    <xsl:text>Les intervenants</xsl:text>
                </title>
                <link rel="stylesheet" type="text/css" href="css/masters.css" />
                <link rel="stylesheet" type="text/css" href="css/menu.css" />
                <script src="script/jquery-was-latest.min.js" type="text/javascript"></script>
                <script src="script/menu.js" type="text/javascript"></script>
            </head>
            <body>
                <div class="main-container">
                <header>
                    <nav class="nav-images">
                        <a href="http://sciences.univ-amu.fr/" class="header-univmed" />
                        <a href="index.html" class="header-masterinfo" />
                        <a href="http://www.univ-amu.fr/" class="header-univprov" />
                    </nav>
                    <nav class="nav-menu">
                        <div id='cssmenu' class="align-center">
                        <ul>
                           <li><a href='index.html'><xsl:text>Le Master</xsl:text></a></li>
                           <li class='has-sub middle-li'><a href='#'><xsl:text>Les formations</xsl:text></a>
                              <ul>
                                  <xsl:call-template name="menu-formation">
                                      <xsl:with-param name="path-parcours">parcours/</xsl:with-param>
                                  </xsl:call-template>
                              </ul>
                           </li>
                           <li class="middle-li"><a href='unités.html'><xsl:text>Les unités</xsl:text></a></li>
                           <li class="active"><a href='intervenants.html'><xsl:text>Les intervenants</xsl:text></a></li>
                        </ul>
                        </div>
                    </nav>
                </header>
                <main>
                <section>
                    <h2 class="align-center">
                        <xsl:text>Les intervenants</xsl:text>
                    </h2>
                </section>
                <section>
                    <h3><xsl:text>Liste des intervenants</xsl:text></h3>
                    <table>
                        <thead>
                            <tr>
                                <th><xsl:text>Nom</xsl:text></th>
                                <th><xsl:text>Page</xsl:text></th>
                                <th><xsl:text>Envoyer un courriel</xsl:text></th>
                            </tr>
                        </thead>
                        <tbody>
                            <xsl:for-each select="/masters/enseignants/enseignant">
                                <tr>
                                    <td>
                                        <xsl:value-of select="prénom" />
                                        <xsl:text> </xsl:text>
                                        <xsl:value-of select="nom" />
                                    </td>
                                    <td>
                                        <a href="enseignants/{fd:formatFileName(concat(nom,'_',prénom))}.html">
                                            <img src="css/img/logo-page.png" alt="fiche_enseignant" />
                                        </a>
                                    </td>
                                    <td>
                                        <a href="mailto:{normalize-space(email)}">
                                            <img src="css/img/logo-mail-to.png" alt="mail_to_enseignant" />
                                        </a>
                                    </td>
                                </tr>
                            </xsl:for-each>
                        </tbody>
                    </table>
                </section>
                </main>
                </div>
            </body>
        </html>
        </xsl:result-document>
    </xsl:template>


    <xsl:template name="menu-formation">
        <xsl:param name="path-parcours" as="xs:string" required="yes"/>
        <xsl:for-each select="/masters/années/année/spécialité">
            <xsl:choose>
                <xsl:when test="count(parcours) = 1">
                    <li><a href="{$path-parcours}{fd:formatFileName(parcours/intitulé)}.html"><xsl:value-of select="parcours/intitulé"/></a></li>
                </xsl:when>
                <xsl:otherwise>
                    <li class='has-sub'><a href="#"><xsl:value-of select="intitulé"/></a>
                        <ul>
                           <xsl:for-each  select="parcours">
                              <li><a href="{$path-parcours}{fd:formatFileName(intitulé)}.html"><xsl:value-of select="intitulé"/></a></li>
                           </xsl:for-each>
                        </ul>
                    </li>
                </xsl:otherwise>
            </xsl:choose>
        </xsl:for-each>
    </xsl:template>


    <xsl:template name="display-resp-lieux">
          <xsl:variable name="varRespIds" select='fd:extractIdrefs(responsables/@idrefs)' />
          <xsl:variable name="listeEnseignants" select="/masters/enseignants" />

            <xsl:choose>
              <xsl:when test="count($varRespIds) = 0">
              </xsl:when>
              <xsl:when test="count($varRespIds) = 1">
                  <p>
                      <b><xsl:text>Responsable</xsl:text></b><xsl:text> : </xsl:text>
                      <u>
                          <xsl:for-each select="$varRespIds">
                              <xsl:variable name="varCurrentId" select='.' />
                               <xsl:for-each select="$listeEnseignants/enseignant[@id = $varCurrentId]">
                                  <a href="../enseignants/{fd:formatFileName(concat(nom,'_',prénom))}.html">
                                      <xsl:value-of select="prénom"/>
                                      <xsl:text> </xsl:text>               
                                      <xsl:value-of select="nom"/>
                                  </a>
                               </xsl:for-each>
                          </xsl:for-each>
                      </u>
                  </p>
              </xsl:when>
              <xsl:otherwise>
                  <p><b><xsl:text>Responsables</xsl:text></b><xsl:text> :</xsl:text>
                      <ul>
                          <xsl:for-each select="$varRespIds">
                              <xsl:variable name="varCurrentId" select='.' />
                               <xsl:for-each select="$listeEnseignants/enseignant[@id = $varCurrentId]">
                                  <li><a href="../enseignants/{fd:formatFileName(concat(nom,'_',prénom))}.html">
                                      <xsl:value-of select="prénom"/>
                                      <xsl:text> </xsl:text>                     
                                      <xsl:value-of select="nom"/>
                                  </a></li>
                               </xsl:for-each>
                          </xsl:for-each>
                      </ul>
                  </p>
              </xsl:otherwise>
          </xsl:choose>

          <xsl:if test="name() = 'parcours'">
              <xsl:choose>
                  <xsl:when test="lieux/@idrefs = 'luminy'">
                      <p><b><xsl:text>Lieu d'enseignement</xsl:text></b><xsl:text>: Campus de Luminy.</xsl:text></p>
                  </xsl:when>
                  <xsl:when test="lieux/@idrefs = 'saint-jérôme'">
                      <p><b><xsl:text>Lieu d'enseignement</xsl:text></b><xsl:text> : Campus de Saint-Jérôme.</xsl:text></p>
                  </xsl:when>
                  <xsl:when test="normalize-space(lieux/@idrefs) = 'luminy saint-jérôme'">
                      <p><b><xsl:text>Lieux d'enseignement</xsl:text></b><xsl:text> : Campus de Luminy et de Saint-Jérôme.</xsl:text></p>
                  </xsl:when>
                  <xsl:when test="normalize-space(lieux/@idrefs) = 'saint-jérôme luminy'">
                      <p><b><xsl:text>Lieux d'enseignement</xsl:text></b><xsl:text> : Campus de Luminy et de Saint-Jérôme.</xsl:text></p>
                  </xsl:when>
                  <xsl:otherwise>
                      <p><b><xsl:text>Lieu d'enseignement</xsl:text></b><xsl:text> : inconnu</xsl:text></p>
                  </xsl:otherwise>
              </xsl:choose>
          </xsl:if>

    </xsl:template>


    <xsl:template name="display-formation-tab">
        <table>
            <thead>
              <tr>
              <th>
                  <xsl:text>Années</xsl:text>
              </th>
              <th>
                  <xsl:text>Spécialités</xsl:text>
              </th>
              <th>
                  <xsl:text>Parcours</xsl:text>
              </th>
              </tr>
            </thead>
            <tbody>
                <xsl:for-each select="/masters/années/année/spécialité/parcours">
                    <tr>
                        <xsl:if test="count(preceding-sibling::parcours) = 0">

                            <xsl:if test="count(parent::spécialité/preceding-sibling::spécialité) = 0">
                                <td rowspan="{count(parent::spécialité/parent::année/child::spécialité/child::parcours)}"><xsl:value-of select="parent::spécialité/parent::année/niveau"/></td>
                            </xsl:if>

                            <xsl:variable name="nbr_parcours" select='count(parent::spécialité/parcours)' />

                            <xsl:choose>
                                <xsl:when test="$nbr_parcours = 1">
                                    <td rowspan="{$nbr_parcours}" colspan="2"><a href="parcours/{fd:formatFileName(intitulé)}.html"><xsl:value-of select="intitulé"/></a></td>
                                </xsl:when>
                                <xsl:otherwise>
                                    <td rowspan="{$nbr_parcours}" colspan="1"><a href="specialites/{fd:formatFileName(parent::spécialité/intitulé)}.html"><xsl:value-of select="parent::spécialité/intitulé"/></a></td>
                                </xsl:otherwise>
                            </xsl:choose>

                        </xsl:if>

                        <xsl:if test="count(parent::spécialité/parcours) &gt; 1">
                            <td><a href="parcours/{fd:formatFileName(intitulé)}.html"><xsl:value-of select="intitulé"/></a></td>
                        </xsl:if>
                    </tr>
                </xsl:for-each>
            </tbody>
        </table>
    </xsl:template>


    <xsl:template name="programme">
        <xsl:param name="path-parcours" required="yes"/>
        <section>
            <h3><xsl:text>Programme des enseignements</xsl:text></h3>
            <xsl:for-each select="$path-parcours/semestre">
                <p>
                    <span class="bold">
                        <xsl:text>Programme du semestre S</xsl:text>
                        <xsl:value-of select="numéro"/>
                        <xsl:text> :</xsl:text>
                    </span>

                    <xsl:variable name="varUEs" select='/masters/UEs' />
                    <ul>
                        <li>
                            <span class="italic"><xsl:text>Unités obligatoires</xsl:text></span>
                            <ul>
                                <xsl:for-each select="fd:extractIdrefs(listUEs/@idrefs)">

                                    <xsl:variable name="varCurrentId" select='.' />
                                    <xsl:variable name="varCurrentUE" select='$varUEs/UE[@id = $varCurrentId and @type = "obligatoire"]' />
                                    
                                        <xsl:if test="$varCurrentUE">
                                            <li>
                                                <a href="../UEs/{fd:formatFileName($varCurrentUE/@id)}.html"><xsl:value-of select="$varCurrentUE/intitulé"/></a>
                                                <xsl:text> (</xsl:text>
                                                <xsl:value-of select="$varCurrentUE/crédits"/>
                                                <xsl:text> crédits)</xsl:text>
                                            </li>
                                        </xsl:if>
                                        
                                </xsl:for-each>
                            </ul>
                        </li>

                        <xsl:variable name="var-ue-optionnelles">
                            <xsl:for-each select="fd:extractIdrefs(listUEs/@idrefs)">

                                <xsl:variable name="varCurrentId" select='.' />
                                <xsl:variable name="varCurrentUE" select='$varUEs/UE[@id = $varCurrentId and @type = "optionnelle"]' />
                                
                                    <xsl:if test="$varCurrentUE">
                                        <li>
                                            <a href="../UEs/{fd:formatFileName($varCurrentUE/@id)}.html"><xsl:value-of select="$varCurrentUE/intitulé"/></a>
                                            <xsl:text> (</xsl:text>
                                            <xsl:value-of select="$varCurrentUE/crédits"/>
                                            <xsl:text> crédits)</xsl:text>
                                        </li>
                                    </xsl:if>
                                    
                            </xsl:for-each>
                        </xsl:variable>

                        <xsl:if test="$var-ue-optionnelles/node()">
                            <li>
                                <span class="italic"><xsl:text>Unités à choisir</xsl:text></span>
                                <ul>
                                    <xsl:apply-templates select="$var-ue-optionnelles/node()
" />
                                </ul>
                            </li>
                        </xsl:if>
                    </ul>
                </p>
            </xsl:for-each>
        </section>
    </xsl:template>


    <xsl:template name="extractResponsableParcours">
        <xsl:param name="enseignant-id" required="yes"/>
                <xsl:for-each select="/masters/années/année/spécialité/responsables">
                    <xsl:variable name="varCurrentSpé" select='parent::spécialité' />
                    <xsl:for-each select="fd:extractIdrefs(@idrefs)">
                        <xsl:variable name="varCurrentId" select='.' />
                        <xsl:if test="$varCurrentId = $enseignant-id">
                            <span>
                                <a href="../specialites/{fd:formatFileName($varCurrentSpé/intitulé)}.html"><xsl:value-of select="$varCurrentSpé/intitulé" /></a>
                                <i><xsl:text> - spécialité</xsl:text></i>
                            </span>
                        </xsl:if>
                    </xsl:for-each>
                </xsl:for-each>

                <xsl:for-each select="/masters/années/année/spécialité/parcours/responsables">
                    <xsl:variable name="varCurrentParcours" select='parent::parcours' />
                    <xsl:for-each select="fd:extractIdrefs(@idrefs)">
                        <xsl:variable name="varCurrentId" select='.' />
                        <xsl:if test="$varCurrentId = $enseignant-id">
                            <span>
                                <a href="../parcours/{fd:formatFileName($varCurrentParcours/intitulé)}.html"><xsl:value-of select="$varCurrentParcours/intitulé" /></a>
                                <i><xsl:text> - parcours</xsl:text></i>
                            </span>
                        </xsl:if>
                    </xsl:for-each>
                </xsl:for-each>
    </xsl:template>


    <xsl:template name="extractParcoursAvecUE">
        <xsl:param name="ue-id" required="yes"/>
            <ul>
                <xsl:for-each select="/masters/années/année/spécialité/parcours/semestre/listUEs">
                    <xsl:variable name="varCurrentParcours" select='ancestor::parcours' />
                    <xsl:for-each select="fd:extractIdrefs(@idrefs)">
                        <xsl:variable name="varCurrentId" select='.' />
                        <xsl:if test="$varCurrentId = $ue-id">
                            <li>
                                <a href="../parcours/{fd:formatFileName($varCurrentParcours/intitulé)}.html"><xsl:value-of select="$varCurrentParcours/intitulé" /></a>
                            </li>
                        </xsl:if>
                    </xsl:for-each>
                </xsl:for-each>
            </ul>
    </xsl:template>

    
    <xsl:function name="fd:extractIdrefs">
        <xsl:param name="list" as="xs:string" />
        <xsl:for-each select=" tokenize($list, ' ') ">
            <xsl:sequence select="." />
        </xsl:for-each>
    </xsl:function>


    <xsl:function name="fd:formatFileName" as="xs:string">
        <xsl:param name="from" as="xs:string" />
        <xsl:sequence select="lower-case(translate(normalize-space($from),' ','_'))" />
    </xsl:function>


    <xsl:template match="br | p | ul | ol | li | b | i | u">

        <xsl:element name="{name()}">
        <xsl:apply-templates/>
        </xsl:element>

    </xsl:template>


    <xsl:template match="a">

        <xsl:element name="a">
            <xsl:if test="@href">
                <xsl:attribute name="href">
                    <xsl:value-of select="@href"/>
                </xsl:attribute>
            </xsl:if>

            <xsl:apply-templates/>
        </xsl:element>

    </xsl:template>

</xsl:stylesheet>
