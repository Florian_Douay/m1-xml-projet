

I-> Généralités

  Le projet se compose d'un fichier XML crée de toutes pièces. Il est composé de plusieurs parties distinctes que sont :

    -une section sur le master en général, comprennant intitulé, présentation et débouchées

    -une section établissements contenant simplement les établissemens avec leur id, nom et ville

    -une section enseignants contenant l'ensemble des enseignants, leur nom, leur prénom, leur id, leur adresse e-mail

    -une section années qui contient successivement les années (M1 ou M2), les spécialités, les parcours, puis les semestres, les UEs associées...

     les informations sur les parcours et les semestres, leurs responsables et leurs lieux d'enseignements sont stockées respectivement à leur niveau

    -enfin une section UEs qui contient l'ensemble des UEs, leur crédits, leur lieux d'enseignement, description...

  Je n'ai pas eu le temps de reconstituer tous les liens entre les noeuds. Par exemple j'ai renseigné seulement une trentaine de professeurs
  et le lien entre professeur-UEs n'a malheureusement pas eu le temps d'être fait sur le site

  La description de l'ensemble des parcours et des spécialités est exaustif. Seule la partie description de la spécialité SIS
  n'apparait pas sur le site, du à un choix d'implémentation court terme (étant donné qu'il n'y a qu'une seul parcours associé à la spécialité,
  ce dernier est proposé directement).

  --------------------------------------------------------------------------------

  La DTD est fournie. Même si elle permet de controller efficacement la plupart des éléments, elle reste à mes yeux insatisfaisante
  dans le sens où elle ne permet pas de détecter certaines mauvaises utilisations de balises html. 
  Exemple : un <li> hors d'un <ul> ou <ol> n'est pas détecté.

  J'ai fourni en annexe (en bas de ce document) mes recherches infructueuses sur un moyen de contourner ce problème.
  Il est fort probable que je sois parti dans une mauvaise direction.

  --------------------------------------------------------------------------------

  J'ai généré avec succès le schéma xsd. J'ai étudié sa structure mais ne pense pas à avoir optimisé suffisament.

  J'ai dans les grandes lignes :

  -remplacé la plupart des xs:string par des xs:token

  -remplacé xs:string par xs:positiveInteger dans le cas du nombre de crédits et du numéro de semestre

  -J'ai ajouté une restriction à partir du type simple xs:token, tel que whiteSpace value="collapse",
   ce qui permet de supprimer les éventuels espaces pour les adresses e-mails

  Je pense que la prise en compte de ce xsd par saxon lors de la transformation XSL(web) aurait été la bienvenue,
  mais pour ce faire il aurait fallu Saxon-EE. Nous sommes malheureusement limité à Saxon-HE.

  --------------------------------------------------------------------------------

  J'ai essayé de reproduire le plus fidèlement possible les fonctionnalités du site du Master.

  Le code n'est pas parfaitement propre. Une passe de factorisation du code serait extrêmement bénéfique
  à la lisibilité.

  Enfin, je ne pense pas avoir réussi à utiliser toutes les bonnes pratiques et les possibilités offertes par XSL 2.0.
  Je pense que certaines choses auraient pu être faite plus simplement ou au moins plus naturellement.

  J'ai laissé ci-dessous (II) la liste des choses restantes à faire

  --------------------------------------------------------------------------------

  J'ai fais le choix de conviction de cibler le standard html5 au lieu de XHTML car c'est la nouvelle norme
  (pour l'instant) et que j'essaye de m'habituer à bien utiliser les nouvelles possibilités, dont les balises 
  sémantiques.

  Même si cela m'a posé bien des embûches, je suis parvenu à générer du html5 entièrement valide
  à partir de mon fichier XSL et de ma requête XQuery.

  Etant donné que tidy ne supporte pas le html5, j'ai tout simplement utilisé le plugin intégré à
  firefox en le configurant sur "validateur w3c en ligne". Ainsi, j'ai bénéficié d'une validation
  html5.

  Il existe apparement un fork de tidy supportant le html5. Voici le lien : https://github.com/htacg/tidy-html5/blob/master/README.md
  Je ne m'y suis pas plus intéressé.

  J'ai tout de même laissé dans le MakeFile la commande tidy. On peut voir qu'elle détecte essentiellement des balises inconnues,
  qui sont tout simplement les nouvelles d'html5.
  
  --------------------------------------------------------------------------------

  Sur le site web, j'ai crée de toutes pièces un CSS en m'inspirant visuellement du site du Master.

  J'ai néanmoins réutilisé le code css du header du site officiel (les 3 images).

  J'ai enfin utilisé un modèle externe pour le menu, afin que ce dernier soit plus dynamique.

  Voici le menu : Flat jQuery Responsive Menu : source : http://cssmenumaker.com/menu/flat-jquery-responsive-menu

  J'ai du néanmoins passer un temps conséquent à l'adapter à mon besoin et à le rendre conforme à la charte graphique.

  --------------------------------------------------------------------------------

  Je n'ai malheureusement pas eu le temps de me pencher sur la partie JAVA. Même si c'était un des objectifs du projet, j'ai essayé
  de relativiser ce choix par le fait que je vais être amené à le faire durant de mon stage cette année.
  J'ai donc privilègié le XSL quite à en faire le choix.



II-> Ce qu'il manque / Ce qu'il serait intéressant de faire

  Parmis les choses que je n'ai pas eu le temps de finaliser figurent :

  -Le comptage total des crédits à "choisir" parmis les options d'un semestre

  -Les unités à enseignement bonus et les tronc communs

  -Séparer proprement le nom des parcours de leur code

  -Rendre le site plus responsive avec des media queries (les efforts sur les % en css ne sont pas totalement suffisants)

  -Corriger le fait que la présentation de la spécialité SIS n'est pas présentée sur le tableau principal

  -Factoriser massivement le code XSL

  -Faire la liaison entre intervenant et UEs

  -Compléter la liste des intervenants

  -Corriger un bug graphique mineur lors des hover sur le menu déroulant du menu



III-> Liens Utiles

  Le dépot git : https://gitlab.com/Florian_Douay/m1-xml-projet



IV-> Annexe recherches DTD

Je suis allé droit dans le mur car je me suis vite rendu comte que les définitions cycliques ne semblent pas être possibles.

<!--<!ENTITY % html-all         "table | tr | td | th | br | p | ul | ol | li | b | i | u | a">-->
<!ENTITY % html-independant "br | b | i | u">
<!ENTITY % html-constraint  "%html-table; | %html-ul-ol; | %html-a; | %html-p;">


<!ENTITY % html-for-th-td   "%html-constraint; | %html-independant;">
<!ENTITY % html-th-td       "((th (%html-for-th-td;)) | (td (%html-for-th-td;)))*">
<!ENTITY % html-tr          "tr (%html-th-td;)+">
<!ENTITY % html-table       "table (%html-tr;)+">


<!ENTITY % html-for-li      "%html-constraint; | %html-independant;">
<!ENTITY % html-li          "(li (%html-for-li;))*">
<!ENTITY % html-ul-ol       "(ul (%html-li;)+) | (ol (%html-li;)+)">


<!ENTITY % html-for-a       "%html-table; | %html-ul-ol; | %html-p; | %html-independant;">
<!ENTITY % html-a           "a (%html-for-a;)*">

<!ENTITY % html-for-p       "%html-table; | %html-ul-ol; | %html-a; | %html-independant;">
<!ENTITY % html-p           "p (%html-for-p;)*">


<!ENTITY % html             "%html-constraint; | %html-independant;">



